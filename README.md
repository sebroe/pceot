
Welcome to the public repository for the thesis titled "Providing Causal Explanations Over Time: An Extension of SCE for Time-Series Data" by Sebastian Rödling. 

This repository contains both the figure materials used in the thesis and the complete code base for the research conducted.
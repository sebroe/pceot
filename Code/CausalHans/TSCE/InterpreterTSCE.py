import numpy as np 
from Utils.logger import get_spec_logger
from .CausalNodeTSCE import *
from .pronunciation_helper import * 
from .tree_helper import *

logger = get_spec_logger(__name__)


import pandas as pd 

class InterpreterTSCE():

    def __init__(self):
        self.causalGraph = None # Override this 
        self.data = None # Override this 

        self.R = lambda x,y: x < y
        self.R_desc = "below average"
        self.S1 = [lambda x,y: x < y, lambda x,y: x > y] # Scenario 2, R1 =: x > y, R2 =: x < y
        self.S2 = [lambda x,y: x > y, lambda x,y: x < y] # Scenario 1, R1 =: x < y, R2 =: x > y
        self.outcomes = [(self.S1, 1), (self.S2, -1)]

        self.i2w = {
            (1,0): "because of his high",
            (-1,0): "because of his low",
            (0,1): "although his high",
            (0,-1): "although his low"
        }

        self.i2wAnt = {
            (1,0): "because of his high",
            (-1,0): "because of his low",
            (0,1): "although his high",
            (0,-1): "although his low"
        }



    def _get_valid_question_ts_and_person(self, variable:str, num=1):

        cExamples = [] 

        for cPerson in reversed(self.data.rollout.keys()):
            for cTimestep in self.data.rollout[cPerson].keys():
                if self.validateQ(cPerson, cTimestep, variable, verbose=False):
                    cExamples.append((cPerson, cTimestep))
                    if len(cExamples) == num:
                        return list(sorted(cExamples, key=lambda x: x[1], reverse=True))

        if cExamples:
            return list(sorted(cExamples, key=lambda x: x[1], reverse=True))


        raise Exception("No valid question found") 
    
    def validateQ(self,  person:int, ts:int, variable:str, verbose=True):

        # We are asking if some variable is below average 
        cPersonValue = self.data.get_person_at_ts_variable(person, ts, variable)
        cPopulationMean = self.data.get_mean(ts, variable)

        if self.R(cPersonValue, cPopulationMean):
            if verbose: print("Question is a True Statement")
            return True 
        else:
            if verbose: raise Exception("Question is a False Statement")

    def _getDirectCauses(self, variable:str): 
        cAllCauses = self.causalGraph[f"0_{variable}"]
        #cAllCauses = cAllCauses.sort_values(ascending=False)
        cAllCauses = cAllCauses.reindex(cAllCauses.abs().sort_values(ascending=False).index) 
        cAllCauses = cAllCauses[cAllCauses != 0]
        return cAllCauses 
    

    def _getDirectEffects(self, variable:str):
        cAllEffects = self.causalGraph.loc[f"-1_{variable}"]
        #cAllEffects = cAllEffects.sort_values(ascending=False)
        cAllEffects = cAllEffects.reindex(cAllEffects.abs().sort_values(ascending=False).index)
        cAllEffects = cAllEffects[cAllEffects != 0]
        return cAllEffects

    def _splitDirectCauses(self, cDirectCauses:pd.Series):
        cIndex = list(cDirectCauses.index)
        cLags = [int(cIndex[i].split("_")[0]) for i in range(len(cIndex))]
        cVarNames = [cIndex[i].split("_")[1] for i in range(len(cIndex))]
        return cLags, cVarNames 
    
    def _splitDirectEffects(self, cDirectCauses:pd.Series):
        cIndex = list(cDirectCauses.index)
        cLags = [int(cIndex[i].split("_")[0]) for i in range(len(cIndex))]

        # Fixing Lags 
        cLagsCorrected = []
        for lag in cLags:
            if lag == 0:
                cLagsCorrected.append(1)
            elif lag == -1:
                cLagsCorrected.append(0)

        cVarNames = [cIndex[i].split("_")[1] for i in range(len(cIndex))]
        return cLagsCorrected, cVarNames 



    def _r1(self, cCausalValueParent, cCausalValueChild, cCausalMuParent, cCausalMuChild, cSign):

        #  self.S1 = [lambda x,y: x < y, lambda x,y: x > y] # Scenario 2, R1 =: x > y, R2 =: x < y
        #  self.S2 = [lambda x,y: x > y, lambda x,y: x < y] # Scenario 1, R1 =: x < y, R2 =: x > y
        #  self.outcomes = [(self.S1, 1), (self.S2, -1)]

        for (R1, R2), outcome in self.outcomes:
            if (cSign < 0 and R2(cCausalValueParent, cCausalMuParent) and R1(cCausalValueChild, cCausalMuChild)) or (cSign > 0 and R2(cCausalValueParent, cCausalMuParent) and R2(cCausalValueChild, cCausalMuChild)):
                return outcome
        return 0


    def _r2(self, cCausalValueParent, cCausalValueChild, cCausalMuParent, cCausalMuChild, cSign):
    
        for (R1, R2), outcome in self.outcomes:
            if (cSign > 0 and R2(cCausalValueParent, cCausalMuParent) and R1(cCausalValueChild, cCausalMuChild)) or (cSign < 0 and R2(cCausalValueParent, cCausalMuParent) and R2(cCausalValueChild, cCausalMuChild)):
                return outcome
        return 0 


    def _r3(self, cNode, lag):
        cDirectCauses = self._getDirectCauses(cNode.causalChild)

        if len(cDirectCauses) == 1:
            return 0 

        cIndices = list(cDirectCauses.index)
        cMaxCauseStrength = cDirectCauses.abs().max()
        cStrongestIndices = []
        for cIndex in cIndices:
            cValue = abs(cDirectCauses[cIndex])
            if cValue == cMaxCauseStrength:
                cStrongestIndices.append(cIndex)

        if len(cStrongestIndices) > 1:
            return 0 
        if len(cStrongestIndices) == 1 and cStrongestIndices[0] == f"{lag}_{cNode.causalParent}":
            return 1
        
        return 0


    def _r3Ant(self, cNode, lag):
        cDirectEffects = self._getDirectEffects(cNode.causalChild)

        if len(cDirectEffects) == 1:
            return 0 

        cIndices = list(cDirectEffects.index)
        cMaxCauseStrength = cDirectEffects.abs().max()
        cStrongestIndices = []
        for cIndex in cIndices:
            cValue = abs(cDirectEffects[cIndex])
            if cValue == cMaxCauseStrength:
                cStrongestIndices.append(cIndex)

        if lag == 1:
            mod_lag = 0 
        else:
            mod_lag = -1

        if len(cStrongestIndices) > 1:
            return 0 
        if len(cStrongestIndices) == 1 and cStrongestIndices[0] == f"{mod_lag}_{cNode.causalParent}":
            return 1
        
        return 0


    def _getIndicator(self, cNode, lag):
        """
        Indicator function for the SCI.
        For better understanding of the indicator function:
        [-1, 0, X] -> "because of low"
        [ 1, 0, X] -> "because of high"
        [ 0,-1, X] -> "although the low"
        [ 0, 1, X] -> "although the high"
        """

        cCausalMuChild = self.data.get_mean(cNode.timestepCausalChild, cNode.causalChild)
        cCausalMuParent = self.data.get_mean(cNode.timestepCausalParent, cNode.causalParent)

        cCausalValueParent = self.data.get_person_at_ts_variable(cNode.person, cNode.timestepCausalParent, cNode.causalParent)
        cCausalValueChild = self.data.get_person_at_ts_variable(cNode.person, cNode.timestepCausalChild,  cNode.causalChild)

        cSign = np.sign(self.causalGraph.loc[f"{lag}_{cNode.causalParent}", f"0_{cNode.causalChild}"])
        
        p1 = self._r1(cCausalValueParent, cCausalValueChild, cCausalMuParent, cCausalMuChild, cSign)
        p2 = self._r2(cCausalValueParent, cCausalValueChild, cCausalMuParent, cCausalMuChild, cSign)
        p3 = self._r3(cNode, lag)

        return (p1, p2, p3)

    def _getIndicatorAnticipative(self, cNode, lag):

        cCausalMuChild = self.data.get_mean(cNode.timestepCausalChild, cNode.causalChild)
        cCausalMuParent = self.data.get_mean(cNode.timestepCausalParent, cNode.causalParent)

        cCausalValueParent = self.data.get_person_at_ts_variable(cNode.person, cNode.timestepCausalParent, cNode.causalParent)
        cCausalValueChild = self.data.get_person_at_ts_variable(cNode.person, cNode.timestepCausalChild,  cNode.causalChild)

        if lag == 1:
            mod_lag = -1
        else:
            mod_lag = 0

        cSign = np.sign(self.causalGraph.loc[f"{mod_lag}_{cNode.causalParent}", f"0_{cNode.causalChild}"])
        
        p1 = self._r1(cCausalValueParent, cCausalValueChild, cCausalMuParent, cCausalMuChild, cSign)
        p2 = self._r2(cCausalValueParent, cCausalValueChild, cCausalMuParent, cCausalMuChild, cSign)
        p3 = self._r3Ant(cNode, lag)

        return (p1, p2, p3)


    def getExpTree(self, person:int, ts:int,  variable:str, max_depth:int = 15):

        if not self.validateQ(person, ts, variable, verbose=False):
            return f"Mario's {variable} is not below average."
        
        cMaxDepthTs = ts - max_depth if ts - max_depth > 0 else 0

        def recursion(node: CausalNodeTSCE, ts:int):

            if ts <= cMaxDepthTs:
                return

            cDirectCauses = self._getDirectCauses(node.name)
            cLags, cVarNames = self._splitDirectCauses(cDirectCauses)

            for lag, varName in zip(cLags, cVarNames):

                cNode = CausalNodeTSCE(
                    name=varName, 
                    person=person, 
                    timestepCausalParent=ts-abs(lag),
                    timestepCausalChild=ts,
                    causalParent=varName, 
                    causalChild=node.name,
                    valueCausalChild=self.data.get_person_at_ts_variable(person, ts, node.name),
                    valueCausalParent=self.data.get_person_at_ts_variable(person, ts-abs(lag), varName),
                )

                cNode.indicator = self._getIndicator(cNode, lag)
                cCheckTS = ts - abs(lag)

                cNode.parent = node 
                if not node.containsSimilarSubTree(cNode):
                    recursion(cNode, cCheckTS)


        root = CausalNodeTSCE(name=variable, person=person, timestepCausalChild=ts,  timestepCausalParent=ts)
        recursion(root, ts)

        #print("Finding sequences .. ")
        cSequences = find_sequences(root)
        #print("Finding sequences is done.", len(cSequences), "sequences found")
        return root 
    



    def getExpTreeAnticipative(self, person:int, ts:int,  variable:str, max_depth:int = 15):

        if not self.validateQ(person, ts, variable, verbose=False):
            return f"Mario's {variable} is not below average."
        
        cMaxDepthTs = ts + max_depth if ts + max_depth < 50 else 50

        def recursion(node: CausalNodeTSCE, ts:int):

            if ts >= cMaxDepthTs:
                return

            cDirectEffects = self._getDirectEffects(node.name)
            cLags, cVarNames = self._splitDirectEffects(cDirectEffects)

            for lag, varName in zip(cLags, cVarNames):

                cNode = CausalNodeTSCE(
                    name=varName, 
                    person=person, 
                    timestepCausalParent=ts,
                    timestepCausalChild=ts + abs(lag),
                    causalParent=node.name, 
                    causalChild=varName,
                    valueCausalParent=self.data.get_person_at_ts_variable(person, ts, node.name),
                    valueCausalChild=self.data.get_person_at_ts_variable(person, ts + abs(lag), varName),
                )

                cNode.indicator = self._getIndicatorAnticipative(cNode, lag)
                cCheckTS = ts + abs(lag)

                # Remained the same. 
                cNode.parent = node 
                if not node.containsSimilarSubTreeAnt(cNode):
                    recursion(cNode, cCheckTS)


        root = CausalNodeTSCE(name=variable, person=person, timestepCausalChild=ts,  timestepCausalParent=ts)
        recursion(root, ts)

        #print("Finding sequences .. ")
        cSequences = find_sequences(root)
        #print("Finding sequences is done.", len(cSequences), "sequences found")
        return root 










    def getExplanation(self, node:CausalNodeTSCE):

        cRoot = node.root 
        cVerbose = True 
        rExplanations = [] 
        cExplainedUUIDs = [] 
        cTimeQAsked = node.timestepCausalChild

        def _helperFillerWords(cNode):
            cIndicators = [cChild.indicator for cChild in cNode.children]
            cFillerWords = [] 
            cChildrenSorted = [] 
            if len(set([i[:2] for i in cIndicators])) == 1:
                for cChild in cNode.children:
                    cFillerWords.append(",")
                    cChildrenSorted.append(cChild)

                if len(cFillerWords) > 2:
                    cFillerWords[-2] = " and"
                elif len(cFillerWords) == 2:
                    cFillerWords[0] = " and"

                cFillerWords[-1] = "." 
            else:
                cIndicators = list(set([i[:2] for i in cIndicators]))
                for counter, cIndicator in enumerate(cIndicators):
                    for cChild in [cChild for cChild in cNode.children if cChild.indicator[:2] == cIndicator]:
                        cFillerWords.append(",")
                        cChildrenSorted.append(cChild)

                    if counter != len(cIndicators) - 2:
                        cFillerWords[-1] = " and"

                if len(cFillerWords) > 2:
                    cFillerWords[-2] = ","
                elif len(cFillerWords) == 2:
                    cFillerWords[0] = " and"

                cFillerWords[-1] = "."

            return cFillerWords, cChildrenSorted
        
        def _helperTimeInformations(cNode, askingRoot:bool = False):
            if askingRoot:
                if cNode.timestepCausalParent == cTimeQAsked:
                    return " this year" 
                elif cNode.timestepCausalParent== cTimeQAsked - 1:
                    return " last year"
                else:
                    return f" {cTimeQAsked - cNode.timestepCausalParent} years ago"
            else:
                if cNode.timestepCausalParent == cNode.timestepCausalChild:
                    return " in the same year" 
                elif cNode.timestepCausalParent - 1== cNode.timestepCausalChild:
                    return " the year before"
                else:
                    return f" {abs(cNode.timestepCausalChild - cNode.timestepCausalParent)} year before"
            
        def _helperTimeInformationContinuous(cNode, askingRoot:bool = False):
            if askingRoot:
                if cNode.timestepCausalParent == cNode.timestepCausalChild:
                    return " in the objected year" 
                elif cNode.timestepCausalParent - 1 == cNode.timestepCausalChild:
                    return " continously one year ago"
                else:
                    return f" continously {abs(cNode.timestepCausalParent - cNode.timestepCausalChild)} year ago"
            else:
                if cNode.timestepCausalParent == cNode.timestepCausalChild:
                    return " in the objected year" 
                elif cNode.timestepCausalParent - 1 == cNode.timestepCausalChild:
                    return " continously one year before"
                else:
                    return f" continously {abs(cNode.timestepCausalParent - cNode.timestepCausalChild)} year before"

        def _getExplanation(node: CausalNodeTSCE):

            if node.is_leaf:
                return 
            
            if node.sequenceTag in cExplainedUUIDs:
                for cChild in node.children:
                    _getExplanation(cChild)
                return 
            else:
                cExplainedUUIDs.append(node.sequenceTag)

            cSequenceNodes = findall_by_attr(cRoot, node.sequenceTag, "sequenceTag")
            cSequenceNodes = sorted(cSequenceNodes, key=lambda x: x.timestepCausalParent, reverse=True)
            cMinTS = cSequenceNodes[0].timestepCausalParent
            cMaxTS = cSequenceNodes[-1].timestepCausalParent

            cExplanation = "" 
            cName = "Hans" if node.is_root else "His"

            timefunction = _helperTimeInformations

            def _helperPositioning(node):
                if self.data.get_mean(node.timestepCausalParent, node.name) < self.data.get_person_at_ts(node.timestepCausalParent, node.person)[node.name]:
                    return "above"
                else:
                    return "below"

            if cMinTS == cMaxTS:
                cPrefix = f"{cName} {node.name} was {_helperPositioning(node)} average {_helperTimeInformations(node, askingRoot=True)}"
                
            else:
                cPrefix = f"{cName} {node.name} constantly over the last {cMinTS - cMaxTS + 1} years was {_helperPositioning(node)} average"          
                timefunction = _helperTimeInformationContinuous

            cFillerWords, cChildrenSorted = _helperFillerWords(node)
            if len(cFillerWords) != len(cChildrenSorted):
                raise Exception("Something went wrong with the filler words.")
            
            for cFiller, cChild in zip(cFillerWords, cChildrenSorted):
                cIndicator = cChild.indicator 
                cMostly = " mostly" if cIndicator[2] == 1 else "" 
                cRelation = self.i2w[cIndicator[:2]]
                cExplanation += f"{cMostly} {cRelation} {cChild.name}{timefunction(cChild, False)}{cFiller}"
                _getExplanation(cChild)

            rExplanations.append(cPrefix + cExplanation)


        _getExplanation(node)

        #Cleaning up some stuff 
        rExplanations = [i.replace("  ", " ") for i in rExplanations]
        rExplanations = [i.replace(" ,", ",") for i in rExplanations]
        rExplanations = [i.replace(" .", ".") for i in rExplanations]
        rExplanations = [i.replace(" and.", ".") for i in rExplanations]
        rExplanations = [i.replace(" and,", ",") for i in rExplanations]
        rExplanations = list(reversed(rExplanations))
        return rExplanations 
    


    def getExplanationAnticipative(self, node:CausalNodeTSCE):

        def _positioning(node):
            if self.data.get_mean(node.timestepCausalChild, node.name) < self.data.get_person_at_ts(node.timestepCausalChild, node.person)[node.name]:
                return "above"
            else:
                return "below"
            

        def _timehelper(timing:int):
            if timing == 0:
                return "in the same year"
            elif timing == 1:
                return "next year"
            else:
                return f"{timing} years later"

        def _timehelperEffect(timing:int):
            if timing == 0:
                return "in the same year"
            elif timing == -1:
                return "the year before"

        cRoot = node.root 
        rExplanations = []


        cSequenceTags = list(set([cChild.sequenceTag for cChild in [node] + list(node.descendants) if cChild.sequenceTag != None]))
        print(cSequenceTags, "\n")
        for tag in cSequenceTags:
            cSequenceExplanations = [] 

            cSequencesElements = findall_by_attr(cRoot, tag, "sequenceTag")
            cSequencesElements = sorted(cSequencesElements, key=lambda x: x.timestepCausalParent, reverse=False)

            cSequenceValidity = cSequencesElements[-1].timestepCausalChild - cSequencesElements[0].timestepCausalChild + 1
            cChildTiming = {cChild.name: cChild.timestepCausalChild - cSequencesElements[0].timestepCausalChild for cChild in cSequencesElements[0].children}
            
            cPosParent = _positioning(cSequencesElements[0])
            cPosChildren = {cChild.name: _positioning(cChild) for cChild in cSequencesElements[0].children}
            cExplizitRelations = {cChild.name: self.i2wAnt[cChild.indicator[:2]] for cChild in cSequencesElements[0].children}

            for cChild in cSequencesElements[0].children:


                cExp = "His " + cChild.name + " is " + cPosChildren[cChild.name] + " average " + f"{_timehelper(cChildTiming[cChild.name])} "
                cExp += cExplizitRelations[cChild.name] + " " + cPosParent + " average " + cSequencesElements[0].name
                cExp += f" {_timehelperEffect(cChild.timestepCausalParent- cChild.timestepCausalChild)}"
                cExp += " continously over the next " + str(cSequenceValidity) + " years."
                cSequenceExplanations.append(cExp)

            rExplanations.append(cSequenceExplanations)


        for cExp in rExplanations:
            print(cExp)
        





            # print() 
            # print("Sequence Variables", "Parent:", cSequencesElements[0].name, "Children:", [tmp.name for tmp in cSequencesElements[0].children])
            # print("Sequence Validity", cSequenceValidity, "time steps")
            # print("Sequence Timing relativ to causal Parent", cChildTiming)
            # print("Position of Parent", _positioning(cSequencesElements[0]))
            # print("Positions", {cChild.name: _positioning(cChild) for cChild in cSequencesElements[0].children}) 
            # print("Explizit Realtions", {cChild.name: self.i2wAnt[cChild.indicator[:2]] for cChild in cSequencesElements[0].children})